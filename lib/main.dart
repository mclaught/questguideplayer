import 'package:flutter/material.dart';
import 'package:http_stream_play/ViewPhotoPage.dart';
import 'file:///D:/Work/QuestGuide/http_stream_play/lib/streaming/PhotoPage.dart';
import 'package:http_stream_play/streaming/StreamRecorder.dart';
import 'package:http_stream_play/streaming/camera.dart';
import 'package:http_stream_play/streaming/tlv.dart';
import 'file:///D:/Work/QuestGuide/http_stream_play/lib/streaming/StreamPlayer.dart';
import 'package:opus_flutter/opus_dart.dart';
import 'package:permission_handler/permission_handler.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  StreamPlayer _player;
  Streamrecorder _recorder;
  var _sessionController = TextEditingController();

  final String _url = "130.193.53.36";
  // SoundPlayer _sPlayer = SoundPlayer.noUI(playInBackground: true);

  @override
  void initState() {
    initOpus();

    _sessionController.text = "111";

    _player = StreamPlayer();
    _player.state.listen((value) {
      if(value == StreamingState.Play && _recorder.state.value == StreamingState.Play)
        _recorder.stop();
    });

    _recorder = Streamrecorder();
    _recorder.state.listen((value) {
      if(value == StreamingState.Play && _player.state.value == StreamingState.Play)
        _player.stop();
    });
    _recorder.onPhoto.listen((url) {
      Navigator.of(context).push(MaterialPageRoute(builder: (ctx) => ViewPhotoPage(url: url)));
    });

    init();
  }


  @override
  void dispose() {
    _player.dispose();
    _recorder.dispose();
  }

  void init()async{
    await Permission.storage.request();
    await Permission.microphone.request();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(left: 32, right: 32),
              child: TextFormField(
                controller: _sessionController,
                decoration: InputDecoration(labelText: "Session"),
              )
            ),
            FlatButton(onPressed: (){
              if(!_sessionController.text.isEmpty)
                _player.play(_url, 8085, _sessionController.text);
              // _sPlayer.play(Track.fromURL("http://questguide.taldykin.com:8085/client/111/aac", codec: Codec.aacADTS));
            }, child: Text("Play stream"),),
            FlatButton(onPressed: (){
              _player.stop();
              // _sPlayer.stop();
            }, child: Text("Stop")),
            StreamBuilder(
              builder: (_,snap) => Text(snap.data.toString()),
              initialData: StreamingState.Stop,
              stream: _player.state,
            ),
            Container(
              margin: EdgeInsets.only(top: 32.0),
              child: FlatButton(onPressed: (){
                if(!_sessionController.text.isEmpty)
                  _recorder.record(_url, 8085, "guide", _sessionController.text);
              }, child: Text("Guide")),
            ),
            StreamBuilder<bool>(
              stream: _player.upstreamEnabled,
              initialData: false,
              builder: (context, snapshot) {
                return FlatButton(onPressed: (){
                  if(!_sessionController.text.isEmpty)
                    _recorder.record(_url, 8085, "cliback", _sessionController.text);
                }, child: Text("Client back", style: TextStyle(color: snapshot.data ? Colors.black : Colors.grey),));
              }
            ),
            FlatButton(onPressed: (){
              _recorder.stop();
              // _sPlayer.stop();
            }, child: Text("Stop rec")),
            StreamBuilder(
              builder: (_,snap) => Text(snap.data.toString()),
              initialData: StreamingState.Stop,
              stream: _recorder.state,
            ),
            Container(
              margin: EdgeInsets.only(top: 32.0),
              child: FlatButton(onPressed: ()async{
                String url = await Navigator.of(context).push(MaterialPageRoute(builder: (ctx)=>PhotoPage(_player)));
                print("PHOTO URL: $url");
              }, child: Text("Фото")),
            ),
          ],
        ),
      ),
    );
  }
}
