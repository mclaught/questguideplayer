import 'dart:convert';

import 'package:camera/camera.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http_stream_play/streaming/rest.dart';
import 'package:http_stream_play/streaming/tlv.dart';
import 'package:intl/intl.dart';

import 'StreamPlayer.dart';

class PhotoPage extends StatefulWidget{
  StreamPlayer _player;

  PhotoPage(this._player);

  @override
  State<StatefulWidget> createState() => _PhotoPageState();

}

class _PhotoPageState extends State<PhotoPage> {
  CameraController _controller;
  bool _sending = false;

  @override
  void initState() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    init();
  }

  init()async{
    if(_controller!=null && _controller.value.isInitialized) {
      await _controller.dispose();
      setState(() {});
    }

    var cameras = await availableCameras();
    for(var cam in cameras){
      print("Camera ${cam.name} ${cam.lensDirection}");
      if(cam.lensDirection == CameraLensDirection.back){
        _controller = CameraController(cam, ResolutionPreset.high);
        await _controller.initialize();
        setState(() {});
        return;
      }
    }
  }


  @override
  void dispose() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    _controller?.dispose();
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // App state changed before we got the chance to initialize.
    if (_controller == null || !_controller.value.isInitialized) {
      setState(() {});
      return;
    }
    if (state == AppLifecycleState.inactive) {
      _controller?.dispose().then((value) => setState(() {}));
    } else if (state == AppLifecycleState.resumed) {
      init();
    }
  }

  @override
  Widget build(BuildContext context) {
    if(_controller == null || !_controller?.value.isInitialized)
      return Center(child: CircularProgressIndicator(),);
    else
      return Scaffold(
        body: Stack(
          children: [
            AspectRatio(
              aspectRatio: _controller.value.aspectRatio,
              child: CameraPreview(_controller),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(child: Offstage(),),
                Container(
                  padding: EdgeInsets.all(16),
                  color: Colors.black,
                  child: Center(
                    child: !_sending ? IconButton(icon: Icon(Icons.photo_camera_outlined, color: Colors.white,), iconSize: 64, onPressed: (){
                      _makePhoto();
                    }) : CircularProgressIndicator(),
                  ),
                )
              ],
            )
          ],
        ),
      );
  }

  void _makePhoto()async {
    String fileName = "/sdcard/DCIM/"+DateFormat("y-M-d-H-m-s").format(DateTime.now())+".jpeg";
    await _controller.takePicture(fileName);
    setState(() {
      _sending = true;
    });
    String url = await Rest().upload(fileName);
    if(mounted) {
      StreamPlayer pl = widget._player;
      if (pl != null && pl.state.value == StreamingState.Play) {
        widget._player.sendTLV(TLVSocket.TYPE_PHOTO, utf8.encode(url));
      }
    }
    setState(() {
      _sending = false;
    });
    Navigator.of(context).pop(url);
  }
}