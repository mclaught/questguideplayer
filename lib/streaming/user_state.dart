import 'package:json_annotation/json_annotation.dart';

//flutter pub run build_runner build

part 'user_state.g.dart';

@JsonSerializable()
class UserState{
  int id;
  double lat;
  double lon;
  int flags;


  UserState({this.id = 0, this.lat, this.lon, this.flags = 0});

  factory UserState.fromJson(Map<String, dynamic> json) => _$UserStateFromJson(json);

  Map<String, dynamic> toJson() => _$UserStateToJson(this);
}