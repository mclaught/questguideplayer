import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:flutter_sound/flutter_sound.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http_stream_play/streaming/tlv.dart';
import 'package:http_stream_play/streaming/user_state.dart';
import 'file:///D:/Work/QuestGuide/http_stream_play/lib/streaming/PlayerQueue.dart';
import 'package:opus_flutter/opus_dart.dart';
import 'package:rxdart/rxdart.dart';

class StreamPlayer extends TLVSocket{
  static const SAMPLE_RATE = 24000;
  static const CHANNELS = 2;
  static const FRAME_DUR_MS = 20;
  static const BITRATE = 24000;
  int PACKET_SIZE;

  FlutterSoundPlayer _player;
  StreamController _opusIn = StreamController<Uint8List>();
  Timer _timer;
  StreamSubscription<Position> _posSubscript;
  BehaviorSubject<bool> upstreamEnabled;
  int sender = -1;
  PlayerQueue _playQueue = PlayerQueue();
  int _flags = 0;

  /// Воспроизведение потокового аудио с сервера QuestGuide
  StreamPlayer(){
    PACKET_SIZE = (BITRATE*FRAME_DUR_MS/8000).round();

    upstreamEnabled = BehaviorSubject.seeded(false);

    _opusIn.stream
        .transform(StreamOpusDecoder.bytes(floatOutput: false, copyOutput: false, sampleRate: SAMPLE_RATE, channels: CHANNELS))
        .map( (List<num> event) => Uint8List.fromList(event) )
        .listen((data) {
          try {
            _player.foodSink?.add(FoodData(data));
          }catch(e,s){
            print(e.toString());
            print(s.toString());
          }
        });

    // _player.foodSink.addStream();

        // .listen((Uint8List data) {
        //   // print("PCM data ${data.length}");
        //   _player.feedFromStream(data);
        // });
  }

  /// Запуск воспроизведения
  /// [host] Адрес сервера
  /// [port] TCP-порт
  /// [session] ID сеанса, полученный с сервера БД
  play(String host, int port, String session)async{

    try {
      if(state.value == StreamingState.Play)
        return;

      _playQueue.reset();

      _player = FlutterSoundPlayer();

      await _player.openAudioSession(
        mode: SessionMode.modeSpokenAudio,
      );

      await connect(host, port, "client", session);//Socket.connect(host, port);

      await _player.startPlayerFromStream(
          numChannels: CHANNELS,
          sampleRate: SAMPLE_RATE,
          codec: Codec.pcm16
      );

      _timer = Timer.periodic(Duration(milliseconds: FRAME_DUR_MS), (timer) {
        if(_playQueue.available()) {
          _opusIn.add(_playQueue.pull());
        }
      });

      //Location
      LocationPermission permission = await Geolocator.checkPermission();
      if(permission == LocationPermission.denied || permission == LocationPermission.deniedForever){
        permission = await Geolocator.requestPermission();
      }
      if(permission != LocationPermission.denied && permission != LocationPermission.deniedForever){
        _posSubscript = Geolocator.getPositionStream(
            desiredAccuracy: LocationAccuracy.best
        ).listen((Position pos) {
          print("Locarion: ${pos.toString()}");

          var js = jsonEncode(UserState(lat: pos.latitude, lon: pos.longitude, flags: _flags).toJson());

          sendTLV(TLVSocket.TYPE_STATE, Uint8List.fromList(js.codeUnits));

          // Float64List coord = Float64List(2);
          // coord[0] = pos.latitude;
          // coord[1] = pos.longitude;
          //
          // sendTLV(TLVSocket.TYPE_STATE, coord.buffer.asUint8List());
        });
      }
    }catch(e,s){
      print(e.toString());
      print(s.toString());
      addState(StreamingState.Error);
    }
  }

  @override
  onClose()async{
    _posSubscript?.cancel();
    _timer.cancel();
    if(_player != null) {
      await _player.stop();
      await _player.closeAudioSession();
      _player = null;
    }
  }

  // void _addData(Uint8List data)async {
  //   print("OPUS data ${data.length}");
  //
  //   int i=0;
  //   while(i<data.length) {
  //     int tp = data[0];
  //     i += 1;
  //     int sz = (data[2] << 8) | data[1];
  //     i += 2;
  //     Uint8List pack = data.sublist(i,i+sz);
  //     i += sz;
  //     _queue.add(pack);
  //     if(_queue.length >= PREBUF)
  //       _queueActive = true;
  //
  //     print("QUEUE ${_queue.length}");
  //   }
  // }

  @override
  void onTLV(int type, Uint8List data) {
    switch(type){
      case TLVSocket.TYPE_OPUS:
        _playQueue.push(data);
        break;

      case TLVSocket.TYPE_SENDER:
        if(data[0] != sender){
          sender = data[0];
          upstreamEnabled.add(sender == 0);
        }
        break;
    }
  }

  void setFlags(int flags)async{
    _flags = flags;
    var pos = await Geolocator.getLastKnownPosition();
    var js = jsonEncode(UserState(lat: pos.latitude, lon: pos.longitude, flags: _flags).toJson());
    sendTLV(TLVSocket.TYPE_STATE, Uint8List.fromList(js.codeUnits));
  }

  void dispose() {
    if(state.value == StreamingState.Play) {
      onClose();
    }
  }
}
