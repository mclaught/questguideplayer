import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart';

class Rest{
  static const URL = "http://questguide.taldykin.com:8080";

  Future<String> upload(String filename)async{
    var basename = filename.split("/").last;
    String folder = "user_photos";

    var request = MultipartRequest('POST', Uri.parse(URL+"/img_upload.php"));
    request.files.add(
        MultipartFile(
            'file',
            File(filename).readAsBytes().asStream(),
            File(filename).lengthSync(),
            filename: basename
        )
    );
    request.fields['folder'] = folder;
    var res = await request.send();

    print("UPLOAD: ${res.statusCode} ${res.reasonPhrase}");
    print(res.stream.listen((value) {
      print("UPLOAD: "+String.fromCharCodes(value));
    }));

    if(res.statusCode >= 300)
      throw Exception(res.reasonPhrase);

    return "${URL}/${folder}/${basename}";
  }
}