import 'dart:io';

import 'dart:typed_data';

import 'package:rxdart/rxdart.dart';
import 'package:wakelock/wakelock.dart';

abstract class TLVSocket{
  static const TYPE_OPUS = 1;
  static const TYPE_STATE = 2;
  static const TYPE_SENDER = 3;
  static const TYPE_PHOTO = 4;
  static const USER_FLAG_REQUEST = 1;

  Socket _socket;
  bool _headerDone = false;
  BehaviorSubject<StreamingState> state;

  TLVSocket(){
    state = BehaviorSubject.seeded(StreamingState.Stop);
  }

  Future<void> connect(host, port, String type, String session)async{
    try {
      _headerDone = false;

      _socket = await Socket.connect(host, port);
      if(_socket == null){
        addState(StreamingState.Error);
        return null;
      }

      _socket.listen(_onSocketData,
          onError: (error) {
            print("SOCKET ERROR ${error.toString()}");
            addState(StreamingState.Error);
            onClose();
          },
          onDone: () {
            print("SOCKET DONE");
            addState(StreamingState.Stop);
            onClose();
          }
      );

      String outHeader = "POST /$type/$session/opus HTTP/1.1\r\n"
          "User-Agent: stagefright/1.2 (Linux;Android 10)\r\n"
          "Host: $host:$port\r\n"
          "Connection: Keep-Alive\r\n\r\n";
      _socket.write(outHeader);
      _socket.flush();

      addState(StreamingState.Play);

      Wakelock.enable();

      return _socket;
    }catch(e,s){
      print(e.toString());
      print(s.toString());
      addState(StreamingState.Error);
      return null;
    }
  }

  stop()async{
    Wakelock.disable();
    addState(StreamingState.Stop);
    onClose();
    _socket.close();
  }

  int sendTLV(int type, Uint8List data){
    Uint8List packet = Uint8List(3+data.length);
    packet[0] = type;
    packet[1] = data.length & 0xFF;
    packet[2] = (data.length & 0xFF00) >> 8;
    packet.buffer.asUint8List(3).setRange(0, data.length, data);

    if(state.value == StreamingState.Play)
      _socket.add(packet);
  }

  void processTLV(Uint8List tlv){
    int i=0;
    while(i<tlv.length) {
      int tp = tlv[i];
      i += 1;
      int sz = (tlv[i+1] << 8) | tlv[i];
      i += 2;
      Uint8List pack = tlv.sublist(i,i+sz);
      i += sz;

      onTLV(tp, pack);
    }
  }

  void addState(StreamingState newState){
    state.add(newState);
  }

  void _onSocketData(Uint8List data) {
    // print("SOCKET: ${data.length}");
    if(!_headerDone){
      String header = String.fromCharCodes(data);
      print(header);
      if(header.contains("\r\n\r\n")){
        _headerDone = true;
        int n = header.indexOf("\r\n\r\n")+4;
        Uint8List subdata = data.sublist(n);

        processTLV(subdata);
      }
    }else{
      processTLV(data);
    }
  }

  void onTLV(int type, Uint8List data);

  void onClose();
}

enum StreamingState{
  Play,
  Stop,
  Error
}