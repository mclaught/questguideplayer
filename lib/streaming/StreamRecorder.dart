import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter_sound/flutter_sound.dart';
import 'package:http_stream_play/streaming/tlv.dart';
import 'package:http_stream_play/streaming/user_state.dart';
import 'package:opus_flutter/opus_dart.dart';
import 'package:rxdart/rxdart.dart';

class Streamrecorder extends TLVSocket{
  static const PREBUF = 20;
  static const SAMPLE_RATE = 16000;
  static const CHANNELS = 1;
  static const FRAME_DUR_MS = 20;
  static const BITRATE = 48000;
  int PACKET_SIZE;
  final FlutterSoundRecorder _recorder = FlutterSoundRecorder();
  StreamController<FoodData> _opusIn = StreamController<FoodData>();
  StreamController<List<int>> _opusOut = StreamController<List<int>>();
  StreamController<UserState> _clientState = StreamController<UserState>();
  StreamController<String> _onPhoto = StreamController<String>();

  Stream<UserState> get clientState => _clientState.stream;
  Stream<String> get onPhoto => _onPhoto.stream;

  Streamrecorder(){
    PACKET_SIZE = (BITRATE*FRAME_DUR_MS/8000).round();

    _opusIn.stream.listen((FoodData event) {
      if(CHANNELS == 2) {
        // Uint16List data16 = event.data.buffer.asUint16List(0);
        Uint8List resmpl = Uint8List(event.data.length*2);
        for(int i=0; i<event.data.length/2; i++) {
          resmpl[i*4] = event.data[i*2];
          resmpl[i*4+1] = event.data[i*2+1];
          resmpl[i*4+2] = event.data[i*2];
          resmpl[i*4+3] = event.data[i*2+1];
        }
        _opusOut.add(resmpl);// //().cast<int>()
      }else{
        _opusOut.add((event as FoodData).data);
      }
    });
    _opusOut.stream.transform(StreamOpusEncoder<int>.bytes(
      frameTime: FrameTime.ms20,
      floatInput: false,
      sampleRate: SAMPLE_RATE,
      channels: CHANNELS,
      application: Application.voip,
    ))
    // _opusIn.stream.map((event) => (event as FoodData).data as List<int>)
    //   .transform(StreamOpusEncoder<int>.bytes(
    //     frameTime: FrameTime.ms20,
    //     floatInput: false,
    //     sampleRate: SAMPLE_RATE,
    //     channels: CHANNELS,
    //     application: Application.voip,
    //   ))
      .listen((data) {
        sendTLV(TLVSocket.TYPE_OPUS, data);
      });
  }

  record(String host, int port, String type, String session)async{

    await connect(host, port, type, session);//Socket.

    await _recorder.openAudioSession(
        mode: SessionMode.modeSpokenAudio
    );

    _recorder.startRecorder(
      codec: Codec.pcm16,
      sampleRate: SAMPLE_RATE,
      numChannels: 1,
      // bitRate: BITRATE,
      toStream: _opusIn.sink
    );
  }

  @override
  onClose()async{
    try {
      await _recorder.stopRecorder();
      await _recorder.closeAudioSession();
    }catch(e,s){
      print(e.toString());
      print(s.toString());
    }
  }

  @override
  void onTLV(int type, Uint8List data) {
    switch(type){
      case TLVSocket.TYPE_STATE:
        // int id = data.buffer.asInt32List()[0];
        // Float64List coords = data.sublist(4).buffer.asFloat64List();
        // double lat = coords[0];
        // double lon = coords[1];

        String str = String.fromCharCodes(data);
        dynamic js = jsonDecode(str);
        UserState state = UserState.fromJson(js);

        print("Client state: ${state.id} ${state.lat}, ${state.lon}");

        _clientState.add(state);
        break;

      case TLVSocket.TYPE_PHOTO:
        String url = String.fromCharCodes(data);
        _onPhoto.add(url);
        break;
    }
  }

  void dispose() {
    if(state == StreamingState.Play){
      onClose();
    }
  }

}
