import 'package:camera/camera.dart';
import 'package:rxdart/rxdart.dart';
import 'package:intl/intl.dart';

class UserCam{
  List<CameraDescription> cameras;
  CameraDescription backCam;
  CameraController controller;
  BehaviorSubject<bool> ready;

  init()async{
    ready = BehaviorSubject.seeded(false);

    cameras = await availableCameras();
    for(var cam in cameras){
      print("Camera ${cam.name} ${cam.lensDirection}");
      if(cam.lensDirection == CameraLensDirection.back){
        backCam = cam;
        controller = CameraController(backCam, ResolutionPreset.high);
        await controller.initialize();
        ready.add(true);
      }
    }
  }

  dispose()async{
    ready.add(false);
    controller?.dispose();
  }

  String userPhoto(){
    if(controller.value.isInitialized) {
      String fileName = DateFormat("y-M-d-H-m-s").format(DateTime.now());
      controller.takePicture("sdcard/DCIM/$fileName.jpeg");
    }
  }
}