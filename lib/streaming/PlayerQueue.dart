import 'dart:typed_data';

class PlayerQueue{
  static const PREBUF = 40;
  static const MAXBUF = 100;
  static const OVERRUN_FACTOR = 10;

  List<Uint8List> _queue = [];
  bool _queueEnableOut = false;
  bool _overrun = false;
  int _count = 0;

  void push(Uint8List data) {
    if(_queue.length > MAXBUF){
      // _overrun = true;
      while(_queue.length > PREBUF)
        _queue.removeAt(0);
    }
    // if(_overrun){
    //   _count++;
    //   if((_count % OVERRUN_FACTOR) == 0)
    //     _queue.removeAt(0);
    //   if(_queue.length < PREBUF)
    //     _overrun = false;
    // }
    _queue.add(data);
    if(_queue.length >= PREBUF)
      _queueEnableOut = true;
    print("QUEUE: ${_queue.length}");
  }

  bool available() {
    return _queue.length > 0 && _queueEnableOut;
  }

  Uint8List pull() {
    if(!available())
      return null;

    var data = _queue.first;
    _queue.removeAt(0);
    if(_queue.length == 0)
      _queueEnableOut = false;
    return data;
  }

  void reset() {
    _queue.clear();
    _queueEnableOut = false;
  }
}