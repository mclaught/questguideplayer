import 'package:flutter/material.dart';

class ViewPhotoPage extends StatefulWidget{
  final String url;

  const ViewPhotoPage({Key key, this.url}) : super(key: key);

  @override
  _ViewPhotoPageState createState() => _ViewPhotoPageState();
}

class _ViewPhotoPageState extends State<ViewPhotoPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Image.network(widget?.url, fit: BoxFit.cover,),
      ),
    );
  }
}