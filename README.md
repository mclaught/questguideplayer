# http_stream_play

**QuestGuide player**

## Установка

Скопировать содержимое пакета _streaming_ в проект QuestGuide.

В _pubspec.yaml_ добавить следующие пакеты:

dependencies:
- stack_trace: ^1.9.5
- flutter_sound: ^6.3.1+1
- permission_handler: ^5.0.1+1
- opus_flutter: ^1.0.1
- camera: ^0.5.8+11
- geolocator: ^6.0.0
- rxdart: ^0.24.1
- wakelock: ^0.2.0+1
- http: ^0.12.2

dev_dependencies:
- build_runner: ^1.10.3
- json_serializable: ^3.5.0

### Для Android 
В манифест добавить разрешения:
```
    <uses-permission android:name="android.permission.INTERNET" />
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
    <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />
    <uses-permission android:name="android.permission.RECORD_AUDIO" />
    <uses-permission android:name="android.permission.FOREGROUND_SERVICE" />
    <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
    <uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" /> 
```    

В тег <application> добавить:

```
    <application
        ...
        android:networkSecurityConfig="@xml/network_security_config">
        ...
        <meta-data android:name="io.flutter.network-policy" android:resource="@xml/network_security_config"/>
    <\application> 
```

Создать файл _@xml/network_security_config.xml_ со следующим содержимым:
```
<?xml version="1.0" encoding="utf-8"?>
<network-security-config>
    <base-config cleartextTrafficPermitted="true">
        <trust-anchors>
            <certificates src="system" />
        </trust-anchors>
    </base-config>
    <domain-config cleartextTrafficPermitted="true">
        <domain includeSubdomains="true">questguide.taldykin.com</domain>
    </domain-config>
</network-security-config>
```
> **questguide.taldykin.com** заменить на доменное имя потокового сервера

## Применение
### Клиентское приложение
```
    ...
    StreamPlayer _player;
    Streamrecorder _recorder;
    ...

    @override
    void initState() {
        ...
        _player = StreamPlayer();
        _player.state.listen((value) {
        if(value == StreamingState.Play && _recorder.state.value == StreamingState.Play)
            /// Закрываем восходящее соединение при отклытии нисходящего
            _recorder.stop();
        });

        _recorder = Streamrecorder();
        _recorder.state.listen((value) {
        if(value == StreamingState.Play && _player.state.value == StreamingState.Play)
            /// Закрываем нисходящее соединение при отклытии восходящего
            _player.stop();
        });

        init();
        ...
    }

    void init()async{
        await Permission.storage.request();
        await Permission.microphone.request();
    }
```
Клиентское приложение не должно одновременно слушать и транслировать поток. Для контроля состояния плейера и рекордера можно использовать Observable **state**.

Событие **upstreamEnabled** можно использовать для контроля возможности включения восходящего потока для пользователя (вопросы пользователя). Это событие сигнализирует о том, что гид освободил входящий слот сеанса и позволяет пользователю подключиться, чтобы задать вопрос.

Пример:
```
    StreamBuilder<bool>(
        stream: _player.upstreamEnabled,
        initialData: false,
        builder: (context, snapshot) {
        return FlatButton(onPressed: (){
            if(!_sessionController.text.isEmpty)
            _recorder.record("questguide.taldykin.com", 8085, "cliback", _sessionController.text);
        }, child: Text("Client back", style: TextStyle(color: snapshot.data ? Colors.black : Colors.grey),));
        }
    ),
```
Клиентское приложение может подключаться к входящему слоту сервера только с типом **"cliback"** (параметр **type** функции **StreamRecorder.record()**).

### Отправка запроса на перехват трансляции (вопросы пользователя)

```
    ...
    _player.setFlags(TLVSocket.USER_FLAG_REQUEST);
    ...
```

При установке восходящего соединения со стороны пользователя необходимо сбросить флаг запроса:

```
    ...
    _player.setFlags(0);
    ...
```

#### Отправка фотоснимков гиду
```
    ...
    Navigator.of(context).push(MaterialPageRoute(builder: (ctx)=>PhotoPage(_player)));
    ...
```
### Приложение гида
Гид может одновременно подключаться к входящему и исходящему слоту сервера, но такой режим не рекомендуется в следствие существенной задержки трансляции. Рекомендуется использовать **StreamPlayer** и **StreamRecorder** на стороне гида попеременно. Для этого можно использовать события **StreamPlayer.state** и **StreamRecorder.state**.

Гид подключается к входящему слоту сервера с типом **"guide"** 
```
    _recorder.record("questguide.taldykin.com", 8085, "guide", <session id>);
```    

При получении от пользователя запроса на перехват трансляции (пользователь хочет задать вопрос), гид может закончить текущую фразу и отключиться от входящего слота сервера

```
    _recorder.stop();
    _player.play("questguide.taldykin.com", 8085, <session id>);
```
При повторном подключении к серверу, клиентское восходящее соединение принудительно закрывается.

#### Состояние пользователя
Для контроля геолокации клиентов и запросов на перехват трансляции используется поток **StreamRecorder.clientState**
```
    _recorder.clientState.listen((UserState state){
        ...
    });
```    
Класс **UserState** содержит id, координаты клиента и флаги.

На данный момент поддерживаются следующие флаги состояния клиента:

- TLVSocket.USER_FLAG_REQUEST - флаг запроса на перехват трансляции.


